const t = require('./walkerData');
const rootPath = '/api/walkers';
const t1 = t(1);
const t2 = t(2);
const testObj = {
     signup1: {
          id: `Sign up using phone 1`,
          path: `${rootPath}/signup`,
          method: 'post',
          data: t1.create,
          response: t1.createR,
          token: 'ownerToken',
     },
     signup2: {
          id: `Sign up using phone 2`,
          path: `${rootPath}/signup`,
          method: 'post',
          data: t2.create,
          response: t2.createR,
          token: 'ownerToken',
     },
     signin1: {
          id: `signin 1`,
          path: `${rootPath}/signin`,
          method: 'post',
          data: t1.signIn,
     },
     verify1: {
          id: `verify 1`,
          path: `${rootPath}/send/otp`,
          method: 'post',
          data: t1.verify,
          saveToken: 'walkerToken',
     },
     verify2: {
          id: `verify 2`,
          path: `${rootPath}/send/otp`,
          method: 'post',
          data: t2.verify,
     },
     update: {
          id: `update`,
          path: `${rootPath}/1`,
          method: 'put',
          data: t1.update,
          response: t1.updateR,
          token: 'walkerToken',
     },
     getById: {
          id: `get by id`,
          path: `${rootPath}/1`,
          method: 'get',
          response: t1.getR,
          token: 'walkerToken',
     },
     search: {
          id: `search`,
          path: `${rootPath}`,
          method: 'get',
          token: 'walkerToken',
     },
     resendOTP: {
          id: `resend OTP`,
          path: `${rootPath}/resend/phone`,
          method: 'post',
          data: { walkerId: 1 },
     },
};

module.exports = Object.values(testObj);
