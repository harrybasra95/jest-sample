const data = (x = 1) => {
     return {
          create: {
               ...baseData,
               primaryTelephone: `989898288890${x}`,
               deviceId: 'string',
               deviceType: 'web',
               serviceablePincodes: ['144002'],
          },
          createR: {
               id: x,
               deviceId: 'string',
               deviceType: 'web',
               primaryTelephone: `989898288890${x}`,
               ...baseData,
          },
          verify: {
               walkerId: x,
               deviceId: 'string',
               deviceType: 'android',
               activationCode: '00000',
               OTPtype: 'email',
          },
          verifyR: {
               id: x,
               status: 'active',
               deviceId: 'string',
               deviceType: 'android',
          },
          signIn: {
               primaryTelephone: `989898288890${x}`,
               countryCode: '91',
          },
          update: {
               ...baseData,
               countryCode: '911',
          },
          updateR: {
               ...baseData,
               countryCode: '911',
          },
          getR: {
               ...baseData,
               countryCode: '911',
               deviceId: 'string',
               deviceType: 'android',
               serviceablePincodes: [{ id: 1, pincode: '144002', walkerId: x }],
          },
          resend: {
               userId: x,
          },
          forgot: {
               email: `harry.basra95${x}@gmail.com`,
          },
          changePassword: {
               newPassword: 'qwerty2',
          },
          signInNewPass: {
               email: `harry.basra95${x}@gmail.com`,
               password: 'qwerty2',
          },
     };
};

const baseData = {
     firstName: 'Harnarinder',
     lastName: 'Singh',
     salary: '1200',
     appLanguage: 'english',
     address: {
          line1: 'line1',
          line2: 'line2',
          pincode: '144002',
     },
     associatedCities: 'jalandhar,batala',
     emergencyContactName: 'Harry',
     relationship: 'parent',
     emergencyAddress: {
          line1: 'line1',
          line2: 'line2',
          pincode: '144002',
     },
     emergencyPrimaryTelephone: '23132132',
     secondaryTelephone: '98989890',
     emergencySecondaryTelephone: '98989890',
     countryCode: '91',
     educationCertificateImgUrl: 'dummyAddress',
     govermentIdImgUrl:
          'https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg',
     addressProofImgUrl:
          'https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg',
     passbookImgUrl:
          'https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg',
     policeVerificationFormImgUrl:
          'https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg',
};

module.exports = data;
