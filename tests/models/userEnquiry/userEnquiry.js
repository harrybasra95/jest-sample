const t = require('./userEnquiryData');
const rootPath = '/api/userEnquiries';
const t1 = t(1);
const t2 = t(2);
const testObj = {
     create: {
          id: `create`,
          path: `${rootPath}`,
          method: 'post',
          data: t1.create,
          response: t1.createR,
          token: 'ownerToken',
     },
     create2: {
          id: `create 2`,
          path: `${rootPath}`,
          method: 'post',
          data: t2.create,
          response: t2.createR,
          token: 'ownerToken',
     },
     update: {
          id: `update`,
          path: `${rootPath}/1`,
          method: 'put',
          data: t1.update,
          response: t1.updateR,
          token: 'ownerToken',
     },
     getById: {
          id: `get by id`,
          path: `${rootPath}/1`,
          method: 'get',
          response: t1.updateR,
          token: 'ownerToken',
     },
     search: {
          id: `search`,
          path: `${rootPath}`,
          method: 'get',
          token: 'ownerToken',
     },
     delete: {
          id: `delete 2`,
          path: `${rootPath}/2`,
          method: 'delete',
          token: 'ownerToken',
     },
};

module.exports = Object.values(testObj);
