const data = (x = 1) => {
     return {
          create: {
               name: 'Harnarinder Singh',
               phone: `989898980${x}`,
               countryCode: '91',
               message: 'this is the message',
               email: 'harry@gmail.com',
          },
          createR: {
               id: x,
               name: 'Harnarinder Singh',
               phone: `989898980${x}`,
               countryCode: '91',
               message: 'this is the message',
               email: 'harry@gmail.com',
          },
          update: {
               phone: `9898989802${x}`,
          },
          updateR: {
               id: x,
               phone: `9898989802${x}`,
          },
     };
};

module.exports = data;
