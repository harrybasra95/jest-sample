const data = (x = 1) => {
     return {
          create: {
               name: 'freeza',
               breedId: x,
               dob: '2013-08-25T17:00:00+00:00',
               gender: 'male',
               isSterilized: false,
               friendlyIndex: 4,
               imgUrl: 'https://example.com',
          },
          createR: {
               id: x,
               name: 'freeza',
               gender: 'male',
               isSterilized: false,
               friendlyIndex: 4,
               imgUrl: 'https://example.com',
          },
          update: {
               name: 'freeza2',
          },
          updateR: {
               id: x,
               name: 'freeza2',
          },
          getR: {
               id: x,
               name: 'freeza2',
               gender: 'male',
               isSterilized: false,
               friendlyIndex: 4,
               imgUrl: 'https://example.com',
               breed: {
                    id: x,
               },
          },
          userGetR: {
               id: x,
               dogs: [
                    {
                         id: x,
                         name: 'freeza2',
                         gender: 'male',
                         isSterilized: false,
                         friendlyIndex: 4,
                         imgUrl: 'https://example.com',
                         breed: {
                              id: x,
                         },
                    },
               ],
          },
     };
};

module.exports = data;
