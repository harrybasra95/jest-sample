const data = (x = 1) => {
     return {
          create: {
               code: 'ABCd',
               discount: '20',
               discountType: 'fixed',
               title: 'New Promo Code',
               description: 'This is the promo code',
               expiryDate: '2022-12-20',
               maxDiscountAmount: 500,
               minOrderTotal: 100,
               useCount: 5,
          },
          createR: {
               id: x,
               code: 'ABCd',
               discount: '20',
               discountType: 'fixed',
               title: 'New Promo Code',
               description: 'This is the promo code',
               maxDiscountAmount: 500,
               minOrderTotal: 100,
               useCount: 5,
          },
          update: {
               discount: '10',
          },
          updateR: {
               id: x,
               discount: '10',
          },
     };
};

module.exports = data;
