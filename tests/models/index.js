const ownerTests = require('./owner/owner');
const walkerTests = require('./walker/walker');
const faqTests = require('./faqs/faq');
const userEnquiryTest = require('./userEnquiry/userEnquiry');
const dogTest = require('./dog/dog');
const timeSlotTest = require('./timeSlot/timeSlot');
const planTest = require('./plan/plan');
const promoCodeTest = require('./promoCode/promoCode');

module.exports = {
     ownerTests,
     walkerTests,
     faqTests,
     userEnquiryTest,
     dogTest,
     planTest,
     timeSlotTest,
     promoCodeTest,
};
