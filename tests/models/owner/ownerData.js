const data = (x = 1) => {
     return {
          create: {
               name: 'Harnarinder Singh',
               email: `harry.basra95${x}@gmail.com`,
               password: 'qwerty',
               timezone: 'Asia/Calcutta',
               phone: '98998980',
               locationCoordinates: { lng: 28.5383355, lat: -81.3792365 },
               appLanguage: 'english',
               address: {
                    line1: 'asdsad',
                    line2: 'asdsad',
                    pincode: '144002',
                    country: 'india',
                    state: 'punjab',
               },
               countryCode: '91',
               deviceId: 'string',
               deviceType: 'android',
          },
          createR: {
               id: x,
               name: 'Harnarinder Singh',
               email: `harry.basra95${x}@gmail.com`,
               timezone: 'Asia/Calcutta',
               phone: '98998980',
               billingEmail: `harry.basra95${x}@gmail.com`,
               countryCode: '91',
               isPhoneVerified: false,
               locationCoordinates: [28.5383355, -81.3792365],
               address: {
                    line1: 'asdsad',
                    line2: 'asdsad',
                    pincode: '144002',
                    country: 'india',
                    state: 'punjab',
               },
               deviceId: 'string',
               deviceType: 'android',
          },
          createFromAdminR: {
               id: x,
               name: 'Harnarinder Singh',
               email: `harry.basra95${x}@gmail.com`,
               timezone: 'Asia/Calcutta',
               phone: '98998980',
               billingEmail: `harry.basra95${x}@gmail.com`,
               countryCode: '91',
               isPhoneVerified: true,
               locationCoordinates: [28.5383355, -81.3792365],
               deviceId: 'string',
               deviceType: 'android',
          },
          verify: {
               ownerId: x,
               deviceId: 'string2',
               deviceType: 'android',
               activationCode: '000000',
          },
          verifyR: {
               id: x,
               isPhoneVerified: true,
               deviceId: 'string2',
               deviceType: 'android',
          },
          signIn: {
               email: `harry.basra95${x}@gmail.com`,
               password: 'qwerty',
          },
          update: {
               countryCode: '919',
               address: {
                    line1: 'asdsad2',
                    line2: 'asdsad2',
                    pincode: '144002',
                    country: 'india',
                    state: 'punjab',
               },
          },
          updateR: {
               countryCode: '919',
               address: {
                    line1: 'asdsad2',
                    line2: 'asdsad2',
                    pincode: '144002',
                    country: 'india',
                    state: 'punjab',
               },
          },
          resend: {
               userId: x,
          },
          forgot: {
               email: `harry.basra95${x}@gmail.com`,
          },
          changePassword: {
               newPassword: 'qwerty2',
          },
          signInNewPass: {
               email: `harry.basra95${x}@gmail.com`,
               password: 'qwerty2',
          },
     };
};

module.exports = data;
