const t = require('./ownerData');
const rootPath = '/api/owners';
const t1 = t(1);
const t2 = t(2);
const t3 = t(3);
const testObj = {
     signup1: {
          id: `Sign up using email 1`,
          path: `${rootPath}/signup`,
          method: 'post',
          data: t1.create,
          response: t1.createR,
     },
     signup2: {
          id: `Sign up using email 2`,
          path: `${rootPath}/signup`,
          method: 'post',
          data: t2.create,
          response: t2.createR,
     },
     signUpUsingAdmin: {
          id: `Sign up from panal `,
          path: `${rootPath}/signup/admin`,
          method: 'post',
          data: t3.create,
          response: t3.createFromAdminR,
     },
     verify1: {
          id: `verify 1`,
          path: `${rootPath}/verify`,
          method: 'post',
          data: t1.verify,
          response: t1.verifyR,
     },
     verify2: {
          id: `verify 2`,
          path: `${rootPath}/verify`,
          method: 'post',
          data: t2.verify,
          response: t2.verifyR,
     },
     signUpUsingSameEmail: {
          id: `Sign up using same email 1`,
          path: `${rootPath}/signup`,
          method: 'post',
          data: t1.create,
          doesFail: true,
     },
     signin1: {
          id: `signin 1`,
          path: `${rootPath}/signin`,
          method: 'post',
          data: t1.signIn,
          saveToken: 'ownerToken',
     },
     signin2: {
          id: `signin 2`,
          path: `${rootPath}/signin`,
          method: 'post',
          data: t2.signIn,
     },
     update: {
          id: `update`,
          path: `${rootPath}/1`,
          method: 'put',
          data: t1.update,
          response: t1.updateR,
          token: 'ownerToken',
     },
     getById: {
          id: `get by id`,
          path: `${rootPath}/1`,
          method: 'get',
          response: t1.updateR,
          token: 'ownerToken',
     },
     search: {
          id: `search`,
          path: `${rootPath}`,
          method: 'get',
          token: 'ownerToken',
     },
     // forgotPassword: {
     //      id: `forgotPassword`,
     //      path: `${rootPath}/forgotpassword`,
     //      method: 'post',
     //      data: { email: t1.createR.email },
     // },
     // updatePassword: {
     //      id: `updatePassword`,
     //      path: `${rootPath}/updatePassword/1`,
     //      method: 'put',
     //      data: t1.changePassword,
     // },
     // signInNewPass: {
     //      id: `sign in using new password`,
     //      path: `${rootPath}/signin`,
     //      method: 'post',
     //      data: t1.signInNewPass,
     // },
};

module.exports = Object.values(testObj);
