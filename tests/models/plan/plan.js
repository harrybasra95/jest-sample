const t = require('./planData');
const rootPath = '/api/plans';
const t1 = t(1);
const t2 = t(2);
const testObj = {
     create: {
          id: `create`,
          path: `${rootPath}`,
          method: 'post',
          data: t1.create,
          response: t1.createR,
          token: 'ownerToken',
     },
     update: {
          id: `update`,
          path: `${rootPath}/1`,
          method: 'put',
          data: t1.update,
          response: t1.updateR,
          token: 'ownerToken',
     },
     getById: {
          id: `get by id`,
          path: `${rootPath}/1`,
          method: 'get',
          response: t1.updateR,
          token: 'ownerToken',
     },
     search: {
          id: `search`,
          path: `${rootPath}`,
          method: 'get',
          token: 'ownerToken',
     },
};

module.exports = Object.values(testObj);
