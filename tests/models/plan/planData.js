const data = (x = 1) => {
     return {
          create: {
               frequency: '1',
               price: 100,
               discountType: 'fixed',
               discount: '20',
               timeInDays: 60,
          },
          createR: {
               id: x,
               frequency: '1',
               price: 100,
               discountType: 'fixed',
               discount: '20',
               timeInDays: 60,
          },
          update: {
               price: 120,
          },
          updateR: {
               id: x,
               price: 120,
          },
     };
};

module.exports = data;
