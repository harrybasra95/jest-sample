const data = (x = 1) => {
     return {
          create: {
               startTime: '18:00:00',
               endTime: '20:00:00',
               type: 'evening',
          },
          createR: {
               startTime: '18:00:00',
               endTime: '20:00:00',
               type: 'evening',
               status: 'active',
          },
          update: {
               startTime: '17:00:00',
          },
          updateR: {
               id: x,
               startTime: '17:00:00',
               endTime: '20:00:00',
               type: 'evening',
               status: 'active',
          },
     };
};

module.exports = data;
