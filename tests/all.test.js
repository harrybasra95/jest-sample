//log = true / for logging body
//saveToken = 'token_name' /  save token in tokenData object for future use
//token = 'token_name' / for including token in header
//skip  = true / to skip running test
//doesFail = true / to check for fail cases

const app = require('../app');
const request = require('supertest');
const database = require('../settings/database');
jest.setTimeout(30000);
const {
     ownerTests,
     walkerTests,
     faqTests,
     userEnquiryTest,
     dogTest,
     timeSlotTest,
     planTest,
     promoCodeTest,
} = require('./models');
const _ = require('underscore');

const testData = [
     {
          name: 'Owner Routes',
          tests: ownerTests,
     },
     // {
     //      name: 'Walker Rountes',
     //      tests: walkerTests,
     // },
     // {
     //      name: 'Faq Rountes',
     //      tests: faqTests,
     // },
     // {
     //      name: 'User Enquiry Rountes',
     //      tests: userEnquiryTest,
     // },
     // { name: 'Dog Routes', tests: dogTest },
     // { name: 'Time Slot Routes', tests: timeSlotTest },
     // { name: 'Plan Routes', tests: planTest },
     // { name: 'PromoCode Routes', tests: promoCodeTest },
];

let tokenData = {};

beforeAll(async () => {
     await database.configure({ force: true });
     db.breed.bulkCreate([
          {
               name: 'pamerian',
          },
          {
               name: 'pug',
          },
     ]);
});
afterAll(async () => {
     try {
          await sequelize.close();
     } catch (error) {
          console.log(error);
     }
});

const runTest = ({
     id,
     path,
     token,
     method,
     saveToken,
     data,
     log,
     skip,
     describeName,
     response,
     doesFail,
}) => {
     if (skip) {
          console.log(`${describeName} ${id} skipped`);
          return;
     }
     test(`${id} ${doesFail ? '(does fail)' : ''}`, async () => {
          const resData = await request(app)
               [method](path)
               .set('x-access-token', tokenData[token] || null)
               .send(data || null);

          const { statusCode, body } = resData;

          if (statusCode !== 200) {
               console.log(body);
          }

          if (doesFail) {
               expect(body.isSuccess).toEqual(false);
               return;
          }

          if (body.isSuccess !== true) {
               console.log(body);
          }
          if (log) {
               console.log(body);
          }
          if (statusCode === 200 && saveToken) {
               tokenData[saveToken] = body.data.token;
          }
          expect(resData.statusCode).toEqual(200);
          expect(body.isSuccess).toEqual(true);
          if (response) {
               expect(body.data).toMatchObject(response);
          }
     });
};

testData.forEach(({ name, tests }) => {
     tests.forEach((data) => {
          data.describeName = name;
          describe(name, () => {
               runTest(data);
          });
     });
});

// const checkData = (data, response) => {
//      Object.keys(response).forEach((key) => {
//           const dataValue = data[key];
//           const responseValue = response[key];
//           if (Array.isArray(dataValue)) {
//                if (_.isObject(dataValue[0])) {
//                     dataValue.forEach((item, i) => {
//                          checkData(item, responseValue[i]);
//                     });
//                } else {
//                     expect(true).toEqual(
//                          _.isEmpty(_.xor(dataValue, responseValue))
//                     );
//                }
//           } else if (_.isObject(dataValue)) {
//                checkData(dataValue, responseValue);
//           } else {
//                expect(dataValue).toEqual(responseValue);
//           }
//      });
// };
